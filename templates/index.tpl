{include file="_head.tpl" title="Documents — %s"|args:$plugin.name current="plugin_%s"|args:$plugin.id}
{include file="%s/templates/_menu.tpl"|args:$plugin_root current="index"}

{form_errors}

{if $list->count()}
    {include file="common/dynamic_list_head.tpl"}

        {foreach from=$list->iterate() item="facture"}
        <tr>
            <td>{$facture.type}</td>
            <th><a href="facture.php?id={$facture.id}">{$facture.numero}</a></th>
            {if $facture.receveur_membre}
                <td>{link href="!users/details.php?id=%d"|args:$facture.receveur_id label=$facture.receveur}</td>
            {else}
                <td>{link href="client.php?id=%d"|args:$facture.receveur_id label=$facture.receveur}</td>
            {/if}
            <td>{$facture.receveur_adresse}</td>
            <td>{$facture.receveur_ville}</td>
            <td>{$facture.date_emission|date:'d/m/Y'}</td>
            <td>{$facture.date_echeance|date:'d/m/Y'}</td>
            <td>{$facture.reglee}</td>
            <td>{$facture.archivee}</td>
            <td>{$facture.moyen_paiement}</td>
            <td>{$facture.contenu|escape|nl2br}</td>
            <td>{$facture.total|escape|money_currency}</td>
            <td class="actions">
                {linkbutton shape="download" href="pdf.php?id=%d&d"|args:$facture.id label="Télécharger"}
                {linkbutton shape="menu" href="facture.php?id=%d"|args:$facture.id label="Voir"}
            </td>
        </tr>
        {/foreach}

    </tbody>
</table>

<p class="help">
    Export de la liste&nbsp;:
    {linkbutton href="?export=csv" label="Export CSV" shape="download"}
    {linkbutton href="?export=ods" label="Export tableur" shape="download"}
</p>
{else}
<p class="help">Aucun document, vous pouvez commencer par {link href="facture_ajouter.php" label="créer un nouveau document"}.</p>
{/if}

{include file="_foot.tpl"}
