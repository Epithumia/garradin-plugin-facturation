<nav class="tabs">
<ul>
    <li{if $current == 'clients'} class="current"{/if}><a href="{$plugin_admin_url}clients.php">Liste clients</a></li>
    <li{if $current == 'client'} class="current"{/if}><a href="{$plugin_admin_url}client.php?id={$client.id}">{$client.nom}</a></li>
    {if $session->canAccess($session::SECTION_ACCOUNTING, $session::ACCESS_WRITE)}
        <li{if $current == 'client_modifier'} class="current"{/if}>
            <a href="{$plugin_admin_url}client_modifier.php?id={$client.id}">Modifier</a></li>{/if}
    {if $session->canAccess($session::SECTION_ACCOUNTING, $session::ACCESS_ADMIN)}
        <li{if $current == 'client_supprimer'} class="current"{/if}>
            <a href="{$plugin_admin_url}client_supprimer.php?id={$client.id}">Supprimer</a></li>{/if}
</ul>
</nav>